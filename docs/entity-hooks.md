# Entity Hooks

Hooks are the primary way you interact with Lua Nova. Entity hooks are
attached to map entities to add behaviour to them, beyond what you can do
with the map editor normally. Unfortunately this is still less than what you
can do with map scripts and the Lua API directly. Still this will handle many
common cases.

A reminder that this does not cover every hook or everything about the hooks
it does cover. But it will give you a lot of context for the big ideas used in
entity hooks.

## Getting to User Properties

1.  Open any map in the map editor.
2.  Select an entity for editing.
3.  Go to the bottom of the panel.

## Trigger Hooks

Trigger hooks each specify something to do. For instance `enable_when` will
enable the entity it is on, while `open_when` opens the door it is on. The
value of the hook says when this should happen.

The value has two parts. The first is the name of the trigger itself and
it gives what kind of thing we are looking for. As example the `switches`
trigger waits for a group of switches to become activated. The second part
isn't always required and can just be left out when it isn't. But when
triggers need more information a colon (`:`) seperates the trigger from the
extra text.

### Opening a Door (Example)

*   Images should be coming in a later version.

We will be using an opening door for this example so now would be a good
time to set up a map and a door. Maybe put some walls around the door for
effect.

Put down one or two enemies on the same side of the door as the hero. Then add
the `open_when` hook with the value `clear_enemies` onto the door. Run the
game and go defeat the enemies. The door should open. If not make sure every-
thing has been saved and you have typed in everything exactly, not even extra
spaces. Also make sure there are no extra enemies on the map otherwise you
will have to slay them too.

If you want there to be extra enemies you don't have to defeat to open this
door then you can use the name prefix. Change the hook's value to
`clear_enemies:in_front` and name the enemies in front of the door
`in_front_1` and (if you put down two) `in_front_2`. You can do this for any
number of enemies and use suffixes like east and west if you like. Now put
down another enemy behind the door and name it `behind_1` or don't name it at
all.

You should be able to run the game, defeat the enemies in front of the door,
have the door open and go through the door to defeat the enemies behind it.

OK enough of enemies. Let's go from action & combat to puzzle solving. Delete
the enemies (or build your doors and walls elsewhere) and put down two
switches. To make this a bit of a puzzle make them walkable switches, don't
require a block but make them deactivate when leaving. Put a block down too.
Now to wire this all together go to the door and it's `open_when` hook. Change
the value to be `switches:`something. Put another name prefix in after the
colon and name the two switches with something that starts with that. Its the
same set up as with the enemies above.

Now go into the game, push the block onto one switch and stand on the other,
the door should be open only once both switches have been pressed.

To up the puzzle level just a bit put a second block down and change the
hook's key from `open_when` to `open_while`. That's it. Do what you did
before, with one block and standing on the second switch. The door should
close when you get off the switch. To keep it open long enough to go through
it use the second block.

To be a true puzzle you might have to add some things around this set up but
it is a start. It also introduces you to the second sub-type of trigger hook
which are covered next.

### One & Two Sided Triggers

Both trigger hooks and triggers themselves can be one- or two- sided. These
"sides" are the points at which things can happen.

The first side is begin, when the thing the trigger is looking for happens
or becomes true. The second side is end, when the event is undone or becomes
false again. One-sided hooks and triggers only deal with the beginning, while
the two-sided versions deal with both.

So one-sided trigger hooks are best for single occurances, particularly ones
that should be locked in place once they have happened. The two-sided trigger
hooks are used for things that can toggle back and forth. So a lot of these
hooks come in pairs, a one-sided hook with `when` in its name and a two-sided
hook with `while` in its name.

Triggers themselves can also be one- or two-sided. However since the second
side of the trigger can just do nothing any triggers that repersent something
that can be undone are two-sided (and can be used with one- or two-sided
hooks) and the rest are one-sided (and can only be used with one-sided hooks).

## Action Hooks

Action hooks are the reverse of trigger hooks. Instead of saying what will
happen and asking for the timing they give timing and you can decide what will
happen at that time.

What happens is a series of actions are executed. The actions are given
in the hook value as a semi-colon (`;`) seperated list. Each action is
given as the action's name then, if more information is provided, a colon
(`:`) and the extra information.

Action hooks are also used in [dialog hooks](dialog-hooks.md) and you can find
out some more about them there.

## Arenas

Arenas are a collection of sensors, foes (enemies) and entrances (doors and
dynamic tiles). They are used to lock the player into a combat encounter
until they have defeated all the enemies.

An arena should be layed out as follows. It should be an enclosed area with
any entrances covered with dynamic tiles and doors with the `arena_entrance`
hook. Just inside those should be sensors marked with `arena_sensor`. All
the enemies you want them to clear to reopen the arena should have the
`arena_foe` hook. Generally this should be all the enemies should be inside
the arena and none of the ones outside of it.
