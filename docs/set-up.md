# Set-Up

This section is all about the initial set-up you have to do for Lua Nova.
For most quests everything here will only have to be done once when you first
start using Lua Nova in the quest and can be ignored after that.

## Getting the Library

First thing is first: How do you actually get the library to use it?

Right now there isn't a nice package for it so you will have to use `Git`,
either directly in the command line or through a graphical application. You
should know just enough about it to know what cloning a repository means and
how to do it with your tool of choice.

#### Directly

This is the simplest but not always the easist, clone the `solarus-lua-nova`
repository onto your computer and copy over the scripts and licenses. The
interface files (`lua-nova.lua` and `global-use.lua`) and the core directory
can go anywhere but must be together and must keep their names. That rule also
works well for the licenses, but they just need to be included and easy to
find.

#### From the Showcase

You can also clone the entire showcase as well. Make sure to get the
submodules as well. If you are using the command line add the
`--recurse-submodules` flag to the clone command. Then you can use Solarus's
import feature to import the `lua-nova` directory, this should pull over all
the scripts and licenses and update the quest database.

#### With Git Submodules

This is only an option if your project is in a Git repository.
Submodules are a less commonly used part of Git so you may have to read
up on it a bit more. I taught myself using the man page and [Git SCM](
https://git-scm.com/book/en/v2/Git-Tools-Submodules).
Use submodule add to make `solarus-lua-nova` a submodule of your project. This
system's strength is it is really easy to get updates because you can just
pull the submodule and commit the change.

## Using it in your Quest

This bit may be the only type you have to open a Lua file while using Lua
Nova. In some place that is run during start-up, such as `main.lua` or file a
`features.lua` file, include the following line:

    require("lua-nova/global-use")

That's it. In the simple case you are done set-up. There are two things
that can make this a bit more complicated. The first is only a bit more
complicated; if you don't install Lua Nova in the same place as the showcase
does that string will change. As an example if you put Lua Nova into the
scripts directory the string will be: `"scripts/lua-nova/global-use"`.

The second is if your quest has a set-up Lua Nova doesn't expect. This can be
a bit more complicated and the compatability section is all about that.

## Compatability

What if Lua Nova doesn't agree with your quest? Because it reaches out and
works with many parts of the engine it is possible that your quest could
introduce something it can't handle; at least not on its own. Here are some
possible issues and how to fix them.

Most of these solutions require more work in Lua to get them working. Usually
because they need more control than the simple interface provided by
global-use gives. If a solution calls for the detailed interface then you can
use the following line to get it:

    local lua_nova = require("lua-nova/lua-nova")

You will have to update the string in the same way as you did with the simple
interface.

#### Multiple Event System Issues

Several multiple-event systems are avalible in the Solarus resource packs. All
of them let you have multiple functions be called when the engine triggers an
event. Lua Nova should be compatable will all of them.

But if events do seem to be getting ignored there a few fixes you can try.
The first is to make sure the simple interface isn't included until after the
multiple-event system is set up. The simple interface can't detect the event-
system if it isn't installed when the set-up happens.

If you aren't using one of the standard multiple-event systems it may not
work with the assumptions of Lua Nova. Lua Nova assumes that the event system
works through a function that can be found on any Solarus userdata metatable
by the name 'register_event'. If your event system does not use this
interface you should create an adaptor function and assign it to
`lua_nova.register_event`.
