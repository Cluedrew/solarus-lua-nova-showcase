# Dialog Hooks

Lua Nova doesn't provide any pretty displays for dialog but it does let you
add behaviour to one. Because of this most projects should include a seperate
dialog system for the pretty display, pick one and follow its intructions
for installation and set up.

The most important thing for working with the dialog hooks is the status
parameter, the one passed from `game:stop_dialog([status])` to the dialog
callback. Look for any documentation about questions in the dialog system.
This exit status is the main way Lua Nova gets information from the dialog
system, so make sure you know what it will be.

## Getting to Dialog Properties

1.  Open the dialogs editor for your language.
2.  Select, or create, a dialog to edit.
3.  The bottom right of the panel is the dialog property editor.

## The dialog Hook

Now we are going to step back to an [entity hook](entity-hooks.md) for a
moment to discuss a little hook called `dialog`. Its actually pretty
complicated. More importantly it is how you start a dialog that works with
Lua Nova; the regular dialog starting doesn't work.

So on any NPC add a hook called `dialog` with a value of any dialog id and
set the NPC's action to "Call the map script". This will have have the same
effect as the "Show a dialog" and entering the dialog into its little box.
(Also you have to call `enable_dialog` if you aren't using the simple
interface.) This will allow the dialog to interact with Lua Nova and have
its hooks be used (and combine it with the `on_interact` event). You can also
use some of the dialog hook's other features to make it dynamically select
which dialog to run.

## Dialog Finished Hooks

There are three hooks that run when a dialog finishes. All three are action
hooks as mentioned in [entity hooks](entity-hooks.md).

Whenever a (Lua Nova) dialog finishes first it will check for the `always`
hook. If present all of the actions given in it will be run. This is your main
hook for running things after the dialog as can run any action.

The other hooks are used for actions that should be run depending on the
value of the dialog status (which in turn usually repersents the answer to
a question or other choice made during the dialog). The reference has a formal
description but here is a brief version. If there is a hook with the name
`if_*` (where the star is replaced by the the dialog status) on this dialog
its actions will be run otherwise the actions of `ifelse` will be run. First
you can have as many different `if_*` hooks as you need to match the different
possibly dialog stutuses. Second the `ifelse` can be left out if their are
no actions to run if no `if_*` matches.

Note that for the conditional hook to ever be matched the dialog status must
be nil, a boolean, a non-negative integer or a string only containing letters,
numbers and underscores. This is due to limits on possible dialog property
keys set by the engine.
