# Introduction

Lua Nova is a tool for level design and content creation within the Solarus
engine. It also allows you to do this without coding in Lua. Instead it uses
other editors to decide what it will do.

The tutorials that follow are mostly designed to give one a sense of the
structure of Lua Nova. A complete listing of every feature of Lua Nova is
included with the library. However that is more of a reference for those who
need to get a few important details. If you are just getting started this
should serve you better.

Examples use Lua Nova itself as well as assets from various resource packs.
Check the license section for details. Most assets can be replaced with any
similar asset, for instance if I put a sprite on a block any sprite that
represents a block should do. Any exceptions to this will be explained when
they appear.

## Should I Use Lua Nova?

Lua Nova isn't needed in every Solarus quest and not everyone is going to
benefit as much from it. Generally Lua Nova is recomended for:

1.  Someone who is new to Solarus and Lua.
2.  A member of a team with mixed skill sets.
3.  Anyone who finds map scripting a bother.

## Tutorials
+   [Set-Up](set-up.md): Getting Lua Nova running in your quest.
+   [Entity Hooks](entity-hooks.md): How to use entity hooks, by example.
+   [Dialog Hooks](dialog-hooks.md): How to use dialog hooks, by example.

## Solarus Resources

If you are having trouble with Solarus itself check the [Solarus Tutorials](
https://www.solarus-games.org/en/development/tutorials) for help. They cover
the basic usage of the engine and editor.

You can also go to the [Solarus Discord](https://discord.com/invite/yYHjJHt)
server to ask questions. This is also the best place to find me for questions
about Lua Nova.
