-- Lua script of item bow.
--
-- Doesn't actually do any bow stuff, its just a sentinal I can check.

local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
    self:set_savegame_variable("bow_variant")
    self:set_amount_savegame_variable("bow_amount")
end

-- Event called when the hero starts using this item.
function item:on_using()
    self:set_finished()
end
