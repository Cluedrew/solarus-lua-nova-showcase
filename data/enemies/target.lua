-- Lua script of enemy target.

local enemy = ...
local sprite

function enemy:on_created()
    sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
    enemy:set_life(1)
    enemy:set_damage(0)
end

function enemy:on_restarted()
    sprite:set_animation"stopped"
end
