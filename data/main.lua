-- From scratch.

require"scripts/no-globals"
require"lua-nova/global-use"

local main = sol.main

function main:on_started()
    local game = sol.game.load"dont_save"
    game:set_ability("sword", 1)
    game:set_starting_location"showcase_1"
    game:set_max_life(5)
    game:set_life(5)
    game:start()
end

-- Module like format, but it isn't.
return
