-- Lua Nova Only
-- Disable global assignment to make sure none of the scripts are leaking
-- names. Nothing else should be touching the global metatable in the
-- project either.

setmetatable(_G, {
        __newindex = function(g, key, value)
            error("[Lua Nova Internal] Attempt to set global: " .. key, 2)
        end
    }
)
