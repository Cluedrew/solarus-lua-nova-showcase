# Lua Nova Showcase

This is the showcase for [Lua Nova][]. It is in part a literal showcase for
Lua Nova but it is also the development environment for it and has some
tutorials for how to use Lua Nova. In other words it is the parts of Lua Nova
that you don't want in your quest.

If you want to run the examples yourself, or import Lua Nova from a quest,
you can clone the repository. Make sure to use the `--recurse-submodules` flag
on clone as the library is in a submodule.

If you want to read the tutorials go [here](/docs/introduction.md).

## License

File-by-file license and author information can be found in the quest
database. Most allow for some form of reuse but if you are interested in that
in most cases you are better off going to the projects I got them from:
+   [Lua Nova][]: The library this is built around.
+   [Free Resource Pack][FRP]: The Solarus community's big resource pack.
+   [MIT Starter Quest][ORP]: A resource pack with more permissive licenses.

[Lua Nova]: https://gitlab.com/Cluedrew/solarus-lua-nova/
[FRP]: https://gitlab.com/solarus-games/solarus-free-resource-pack/
[ORP]: https://gitlab.com/Splyth/solarus-mit-starter-quest/
